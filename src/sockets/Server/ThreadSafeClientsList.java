package sockets.Server;

import sockets.Transport.TcpDataTransporter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ThreadSafeClientsList {
    private HashMap<String, TcpDataTransporter> internalClientsList;

    public ThreadSafeClientsList() {
        internalClientsList = new HashMap<>();
    }

    public synchronized void addNewClient(String clientName, TcpDataTransporter transporter) {
        if (!internalClientsList.containsKey(clientName)) {
            internalClientsList.put(clientName, transporter);
        }

    }

    public synchronized void removeClient(String clientName) {
        internalClientsList.remove(clientName);
    }

    public synchronized Integer getSize(){
        return internalClientsList.size();
    }

    public synchronized List<TcpDataTransporter> getAllTransportersExceptFromSpecifiedClient(String clientName) {
        List<TcpDataTransporter> result = new ArrayList<>();
        for(Map.Entry<String, TcpDataTransporter> clientInfoItem : internalClientsList.entrySet()) {
            if (!clientInfoItem.getKey().equals(clientName)) {
                result.add(clientInfoItem.getValue());
            }
        }
        return result;
    }
}
