package sockets.Client;

import sockets.Common.Exceptions.SideOfTransporterNotRealizedException;
import sockets.Common.Exceptions.TransporterIncorrectStateException;
import sockets.Interaces.IDataReceivedListener;
import sockets.Transport.TcpDataTransporter;
import sockets.Transport.TransporterSide;

import java.util.Scanner;

public class ClientServerCommunicationThread extends Thread implements IDataReceivedListener {
    private Integer port;
    private TcpDataTransporter transporter;
    private String clientName;

    public ClientServerCommunicationThread(String clientName, Integer port) {
        this.clientName = clientName;
        this.port = port;
    }

    private void initSocketConnection(){
        transporter = new TcpDataTransporter("localhost", port, TransporterSide.Client);
        try {
            transporter.TryConnect();
            transporter.AddDataReceivedListener(this);
            transporter.StartListenIncomingData();
        } catch (SideOfTransporterNotRealizedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        initSocketConnection();
        System.out.println("Enter messages:");
        while(!isInterrupted()) {
            Scanner scanner = new Scanner(System.in);
            String message = scanner.nextLine();
            if (message.length() == 0) {
                transporter.Disconnect();
                interrupt();
                break;
            }
            try {
                String messageForSend = "ClientName:"+clientName + ";" + message;
                transporter.SendPacket(messageForSend.getBytes());
            } catch (TransporterIncorrectStateException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void DataReceived(String message) {
        System.out.println("Received message: " + message);
    }
}
