package sockets.Transport;

import sockets.Common.Exceptions.SideOfTransporterNotRealizedException;
import sockets.Common.Exceptions.TransporterIncorrectStateException;
import sockets.Interaces.IDataReceivedListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.*;
import java.util.ArrayList;

public class TcpDataTransporter {

    private final int SOCKET_ACCEPT_TIMEOUT = 15000;
    private String address;
    private int port;
    private TransporterSide createdSide;
    private Socket socket;
    private DataInputStream inputStream;
    private DataOutputStream outputStream;
    private Boolean isConnected;
    private ArrayList<IDataReceivedListener> listeners;
    private Object lockObject;

    public TcpDataTransporter(String address, int port, TransporterSide whereTransporterCreated) {
        lockObject = new Object();
        this.address = address;
        this.port = port;
        createdSide = whereTransporterCreated;
        isConnected = false;
        listeners = new ArrayList<>();
    }

    private void Log(String message)
    {
        //System.out.println("TcpTR"+ " " + message);
    }

    public Boolean TryConnect()
            throws SideOfTransporterNotRealizedException
        {
        Log("TryConnect");
        switch (createdSide) {
            case Client:
                try {
                    Log("case Client:");
                    Log("address = " + address);
                    Log("port = " + port);
                    socket = new Socket(InetAddress.getByName(address), port);
                    inputStream = new DataInputStream(socket.getInputStream());
                    outputStream = new DataOutputStream(socket.getOutputStream());
                    isConnected = true;
                }catch (IOException ex) {
                    Log("IOException ex");
                    ex.printStackTrace();
                }
                break;
            case Server:
                try{
                    Log("case Server:");
                    Log("Port =" + port);
                    Log("address = " + address);
                    ServerSocket serverSocket = new ServerSocket(port, 0, InetAddress.getByName(address));

                    serverSocket.setSoTimeout(SOCKET_ACCEPT_TIMEOUT);
                    socket = serverSocket.accept();
                    inputStream = new DataInputStream(socket.getInputStream());
                    outputStream = new DataOutputStream(socket.getOutputStream());
                    isConnected = true;
                }catch (IOException ex) {
                    Log("IOException ex");
                   ex.printStackTrace();
                }
                break;
            default:
                throw new SideOfTransporterNotRealizedException("You must realize creation of this transporter on new way");
        }
        Log("TryConnect finish");
        return isConnected;
    }

    public void Disconnect() {
        Log("Disconnect()");
        try{
            inputStream.close();
            outputStream.close();
            socket.close();
        }catch (IOException ex) {
            ex.printStackTrace();
        }
        isConnected = false;
    }

    public void SendPacket(byte[] data) throws TransporterIncorrectStateException {
        Log("SendPacket()");
        synchronized (lockObject) {
            try {
                if (outputStream == null) {
                    Log("_outputStream == null");
                    throw new TransporterIncorrectStateException("Can not send data, because output stream is null");
                }
                outputStream.write(data, 0, data.length);
                outputStream.flush();

            } catch (IOException e) {
                Log("catch (IOException e)");
                e.printStackTrace();
                Log(e.getMessage());
            }
        }

    }

    public void AddDataReceivedListener(IDataReceivedListener listener) {
        synchronized (listeners) {
            if (listeners != null) {
                listeners.add(listener);
            }
        }
    }

    public void RemoveDataReceivedListener(IDataReceivedListener listener) {
        synchronized (listeners) {
            if (listeners != null && listeners.contains(listener)) {
                listeners.remove(listener);
            }
        }
    }

    public void StartListenIncomingData(){
        Log("StartListenIncomingData");
        Thread listenDataThread = new Thread(() -> {
            try {
                socket.setSoTimeout(50);
            } catch (SocketException e) {
                e.printStackTrace();
                isConnected = false;
            }
            while(isConnected) {
                byte[] buffer = new byte[1024];
                int bytesReadedFromStream = 0;
                try {
                    byte receivedByte;
                    try{
                        while ((receivedByte = inputStream.readByte()) != -1) {
                            buffer[bytesReadedFromStream] = receivedByte;
                            bytesReadedFromStream++;
                        }
                    }catch (EOFException ex) {
                        //Log("StartListenIncomingData - EOFException ex");
                    }
                    catch (SocketTimeoutException ex) {
                        //Log("StartListenIncomingData - SocketTimeoutException ex");
                    }
                    catch (SocketException ex) {
                        Log("StartListenIncomingData - SocketException ex");
                        isConnected = false;
                    }
                } catch (IOException e) {
                    Log("StartListenIncomingData - IOException e");
                    e.printStackTrace();
                    continue;
                }

                if (bytesReadedFromStream > 0) {
                    Log("StartListenIncomingData - bytesReadedFromStream > 0");
                    byte[] receivedData = new byte[bytesReadedFromStream];
                    System.arraycopy(buffer, 0, receivedData, 0, bytesReadedFromStream);
                    synchronized (listeners) {
                        for (IDataReceivedListener someDataListener: listeners) {
                            someDataListener.DataReceived(new String(receivedData, 0, receivedData.length));
                        }
                    }
                }
            }
        });
        listenDataThread.start();
    }
}
