package sockets.Server;


import sockets.Common.Exceptions.TransporterIncorrectStateException;
import sockets.Interaces.IDataReceivedListener;
import sockets.Transport.TcpDataTransporter;

import java.util.List;

public class MessageResender implements IDataReceivedListener {
    private ThreadSafeClientsList clientsList;
    public MessageResender(ThreadSafeClientsList clientsList) {
        this.clientsList = clientsList;
    }

    public synchronized void resendMessage(String incomingMessage) {
        String[] splitted = incomingMessage.split(";");
        List<TcpDataTransporter> transporterList = clientsList.getAllTransportersExceptFromSpecifiedClient(splitted[0]);
        String messageText = splitted[1];
        for (TcpDataTransporter transporter : transporterList) {
            try {
                transporter.SendPacket(messageText.getBytes());
            } catch (TransporterIncorrectStateException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void DataReceived(String message) {
        resendMessage(message);
    }
}
