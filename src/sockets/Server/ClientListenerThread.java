package sockets.Server;


import sockets.Common.Exceptions.SideOfTransporterNotRealizedException;
import sockets.Transport.TcpDataTransporter;
import sockets.Transport.TransporterSide;

public class ClientListenerThread extends Thread {

    private String clientName;
    private MessageResender resender;
    private Integer port;
    private TcpDataTransporter transporter;
    private ThreadSafeClientsList clientsList;

    public ClientListenerThread(ThreadSafeClientsList clientsList, String clientName, Integer port, MessageResender resender) {
        this.resender = resender;
        this.port = port;
        this.clientsList = clientsList;
        this.clientName = clientName;
    }

    @Override
    public void run() {
        transporter = new TcpDataTransporter("localhost", port, TransporterSide.Server);
        this.clientsList.addNewClient(clientName, transporter);
        transporter.AddDataReceivedListener(resender);
        try {
            transporter.TryConnect();
        } catch (SideOfTransporterNotRealizedException e) {
            e.printStackTrace();
        }
        transporter.StartListenIncomingData();
    }
}
