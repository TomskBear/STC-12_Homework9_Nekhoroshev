package sockets.Transport;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * Created by MNekhoroshev on 9/6/2018.
 * Utility class which helps to get available port for client/server communication
 */
public class AvailablePortGetter {
    private AvailablePortGetter(){

    }

    /**
     * Method gets available port value
     * @return
     */
    public static Integer getNextAvailablePort(){
        ServerSocket socket = null;
        int result = -1;
        try {
            socket = new ServerSocket(0);
            result = socket.getLocalPort();
            socket.close();
            return result;

        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (socket != null && !socket.isClosed()) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return -1;
    }
}
