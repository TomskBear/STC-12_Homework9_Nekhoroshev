package sockets.Client;

import java.util.Scanner;

public class ClientMain {
    public static void main(String[] args) {
        System.out.println("Specify client name");
        Scanner scanner = new Scanner(System.in);
        String clientName = scanner.nextLine();
        ServerConnectionRequester requester = new ServerConnectionRequester(clientName);
        Integer availablePort = requester.askForConnectionAndGetAvailablePort();
        System.out.println("clientName = " + clientName + " port = " + availablePort);
        ClientServerCommunicationThread communicationThread = new ClientServerCommunicationThread(clientName, availablePort);
        communicationThread.start();
        try {
            communicationThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
