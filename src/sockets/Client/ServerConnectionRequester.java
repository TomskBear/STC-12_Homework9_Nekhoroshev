package sockets.Client;

import sockets.Common.Constants;

import java.io.IOException;
import java.net.*;

import static java.lang.Thread.sleep;

public class ServerConnectionRequester {
    private DatagramSocket socket;
    private InetAddress address;
    private String clientName;
    private byte[] buf = new byte[256];

    public ServerConnectionRequester(String clientName) {
        this.clientName = clientName;
        try {
            socket = new DatagramSocket();
            address = InetAddress.getByName("localhost");
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public Integer askForConnectionAndGetAvailablePort() {
        String resultMessage = "ClientName:" + clientName + ";" + "ConnectClient";
        DatagramPacket packet = new DatagramPacket(resultMessage.getBytes(), 0, resultMessage.length(), address, Constants.PORT_FOR_GET_AVAILABLE_PORT_INFO_VALUE);
        send(packet);
        boolean isMyPacketReceived = false;
        int iterations = 10;
        String availablePortString = null;
        while(!isMyPacketReceived && iterations > 0) {
            packet = receive();
            String receivedMessage = new String(packet.getData(), 0 , packet.getLength());
            String[] splittedString = receivedMessage.split(";");
            availablePortString = splittedString[1];
            String receivedClientName = splittedString[0].split(":")[1];
            if (receivedClientName.equals(clientName)) {
                break;
            }
            try {
                sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            iterations--;
        }
        return Integer.valueOf(availablePortString.split(":")[1]);

    }

    public DatagramPacket receive() {
        DatagramPacket packet
                = new DatagramPacket(buf, buf.length);
        try {
            socket.receive(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return packet;
    }

    private void send(DatagramPacket packet) {
        try {
            socket.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
