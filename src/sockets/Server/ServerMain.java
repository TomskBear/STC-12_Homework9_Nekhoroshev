package sockets.Server;

public class ServerMain {
    public static void main(String[] args) {
        ThreadSafeClientsList clientsList = new ThreadSafeClientsList();
        IncomingConnectionListener incomingConnectionListener = new IncomingConnectionListener(clientsList);
        incomingConnectionListener.start();
    }
}
