package sockets.Server;

import sockets.Transport.AvailablePortGetter;
import sockets.Common.Constants;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class IncomingConnectionListener extends Thread {

    private ThreadSafeClientsList clientsList;
    private MessageResender resender;
    protected DatagramSocket socket;
    private byte[] buf = new byte[256];

    public IncomingConnectionListener(ThreadSafeClientsList clientsList) {
        this.clientsList = clientsList;
        this.resender =  new MessageResender(clientsList);
        try {
            socket = new DatagramSocket(Constants.PORT_FOR_GET_AVAILABLE_PORT_INFO_VALUE);
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    private DatagramPacket receive(){
        DatagramPacket packet
                = new DatagramPacket(buf, buf.length);
        try {
            socket.receive(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return packet;
    }

    private void send(String response, InetAddress address, Integer port) {
        DatagramPacket packet = new DatagramPacket(response.getBytes(), response.length(), address, port);
        try {
            socket.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void processReceivedMessage(DatagramPacket packet, String clientName, String command) {
        System.out.println("ClientNAme =  " + clientName);
        System.out.println("Command =  " + command);
        if (command.equals("ConnectClient")) {
            InetAddress address = packet.getAddress();
            int portForResponseMessage = packet.getPort();
            int incomingDirectClientPort = AvailablePortGetter.getNextAvailablePort();
            String response = clientName + ";" + "Port:" + incomingDirectClientPort;
            ClientListenerThread clientListenerThread = new ClientListenerThread(clientsList, clientName, incomingDirectClientPort, resender);
            clientListenerThread.start();
            send(response, address, portForResponseMessage);

        }
    }

    @Override
    public void run() {
        while(!isInterrupted()) {
            DatagramPacket packet = receive();
            String received
                    = new String(packet.getData(), 0, packet.getLength());
            String[] splitterReceivedMessage = received.split(";");
            processReceivedMessage(packet, splitterReceivedMessage[0],splitterReceivedMessage[1]);
        }
    }
}
