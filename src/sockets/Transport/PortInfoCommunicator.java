package sockets.Transport;

import sockets.Common.Constants;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by MNekhoroshev on 9/6/2018.
 */
public abstract class PortInfoCommunicator {
    protected DatagramSocket socket;
    protected InetAddress address;
    private static final String INFO_SPLITTER = ":";
    private static final String CLIENT_NAME_PREFIX_STRING = "ClientName";
    private static final String CONNECT_CLIENT_COMMAND = "ConnectClient";
    private static final String AVAILABLE_PORT_INFO_RESPONSE = "Port";

    private final String COMMANDS_SPLITTER =";";
    private byte[] buf = new byte[256];


    public PortInfoCommunicator(){
        initSocket();
    }

    public abstract void initSocket();

    private InetAddress getAddress(){
        try {
            return InetAddress.getByName("localhost");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return null;
    }
    private void send(DatagramPacket packet) {
        try {
            socket.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected String getConnectClientCommandString(){
        return CONNECT_CLIENT_COMMAND;
    }

    protected String getClientNamePrefix() {
        return CLIENT_NAME_PREFIX_STRING;
    }

    protected String getInfoSplitter(){
        return INFO_SPLITTER;
    }

    protected String getAvailablePortInfoResponseString(){
        return AVAILABLE_PORT_INFO_RESPONSE;
    }

    protected String getCommandsSplitSymbol(){
        return COMMANDS_SPLITTER;
    }

    protected void send(String message, InetAddress inetAddress, Integer port) {
        DatagramPacket packet = new DatagramPacket(message.getBytes(), message.length(), inetAddress, port);
        send(packet);
    }

    protected void send(String message, InetAddress inetAddress) {
        send(message, inetAddress, Constants.PORT_FOR_GET_AVAILABLE_PORT_INFO_VALUE);
    }

    public void send(String message) {
        if (address == null) {
            address = getAddress();
        }
        send(message, address, Constants.PORT_FOR_GET_AVAILABLE_PORT_INFO_VALUE);
    }

    public DatagramPacket receive() {
        DatagramPacket packet
                = new DatagramPacket(buf, buf.length);
        try {
            socket.receive(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return packet;
    }
}
